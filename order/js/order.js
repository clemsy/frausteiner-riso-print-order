
Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.use(VueFormGenerator);
Vue.use('vue-cookies')

const vm = new Vue({
    el: '#app',
    data: {
        specialOffer: null,
        changes: 0,
        config: [],
        deliveryData: {
            nameFirst:null,
            nameLast:null,
            companyName:null,
            companyVAT:null,
            email:null,
            phone:null,
            geolocalized:null,
            'street-address':"",
            'address-line1':"",
            'address-line2':"",
            'address-line3':"",
            'address-level1':"",
            'address-level2':"",
            'address-level3':"",
            'address-level4':"",
            country: null
        },
        formOptions: {
            validateAfterChanged: true,
            validateAfterLoad: false
        },
        schemaAddress: {
            styleClasses: "input-lg",
            fields: [
                {
                    type: "input",
                    inputType: "text",
                    label: "Firstname",
                    model: "nameFirst",
                    readonly: false,
                    featured: true,
                    required: true,
                    disabled: false,
                    placeholder: "Your firstname",
                    validator: VueFormGenerator.validators.string
                }, {
                    type: "input",
                    inputType: "text",
                    label: "Lastname",
                    model: "nameLast",
                    readonly: false,
                    featured: true,
                    required: true,
                    disabled: false,
                    placeholder: "Your lastname",
                    validator: VueFormGenerator.validators.string
                }, {
                    type: "input",
                    inputType: "text",
                    label: "Company Name",
                    model: "companyName",
                    readonly: false,
                    featured: true,
                    required: false,
                    disabled: false,
                    placeholder: "Company Name",
                    validator: VueFormGenerator.validators.string
                }, {
                    type: "input",
                    inputType: "text",
                    label: "VAT Number",
                    model: "companyVAT",
                    readonly: false,
                    featured: true,
                    required: false,
                    disabled: false,
                    placeholder: "VAT Number",
                    validator: VueFormGenerator.validators.string
                }, {
                    type: "googleAddress",
                    inputType: "text",
                    label: "Your address",
                    model: "geolocalized",
                    readonly: false,
                    featured: true,
                    required: true,
                    disabled: false,
                    placeholder: "Type in your address",
                    onPlaceChanged(value, place, rawPlace, model, schema) {
                        console.log("Location changed! " + value);
                        console.log("Location changed! " + place.country);
                    }
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "street-address",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-line1",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-line2",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-line3",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-level1",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-level2",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-level3",
                    required: false
                },
                {
                    type: "input",
                    inputType: "hidden",
                    visible: false,
                    model: "address-level4",
                    required: false
                },
                {
                    type: "hidden",
                    inputType: "hidden",
                    visible: false,
                    model: "country",
                    required: false
                },
                {
                    type: "input",
                    inputType: "email",
                    label: "Your email address",
                    model: "email",
                    readonly: false,
                    featured: true,
                    required: true,
                    disabled: false,
                    placeholder: "xxx@yyy.zzz",
                    validator: VueFormGenerator.validators.email
                },
                {
                    type: "input",
                    inputType: "text",
                    label: "Your Phone Number",
                    model: "phone",
                    readonly: false,
                    featured: true,
                    required: true,
                    disabled: false,
                    placeholder: "+32 (0) 444 55 66 77",
                    validator: VueFormGenerator.validators.string
                },
                {
                    type: "textArea",
                    label: "Comments",
                    model: "comments",
                    readonly: false,
                    featured: true,
                    required: false,
                    disabled: false,
                    hint: "Max 500 characters",
                    rows:10,
                    placeholder: "Write here your comments.",
                }
                /*, {
                    type: "checklist",
                    label: "Skills",
                    model: "skills",
                    multi: true,
                    required: true,
                    multiSelect: true,
                    values: ["HTML5", "Javascript", "CSS3", "CoffeeScript", "AngularJS", "ReactJS", "VueJS"]
                }, {
                    type: "switch",
                    label: "Status",
                    model: "status",
                    multi: true,
                    readonly: false,
                    featured: false,
                    disabled: false,
                    default: true,
                    textOn: "Active",
                    textOff: "Inactive"
                } */
             ]
        },
        file: "",
        files: [],
        form:  {
            errors: 0,
            errorMessages: {
                paper: false,
                format: false,
                quantity: false
            },
            validators: {
                1: {
                    paper: 'not empty',
                    format: 'not empty',
                    quantity: 'greater than 0'
                },
                2: {
                    archiveUrl: 'not empty',
                    pages: {
                        1: {
                            colors: {
                                recto: ''
                            }
                        }
                    }
                }
            },
            formatIsReduced: false,
            totalPrice: '',
            step: 1,
            type: '',
            // size: 'Carte Postale',
            //format: 'A3',
            format: '',
            //quantity: 1,
            quantity: null,
            paper: '',
            //paper: 'Metapaper Rough Warmwhite 90gsm',
            totalPages: 1,
            pages: [],
            page: {
                name: '',
                files: {
                    recto: { upload: '', url: '' },
                    verso: { upload: '', url: '' }
                },
                colors: {
                    //recto: ["black", "orange"],
                    recto: [],
                    verso: [],
                },
                hasVerso: false,
                isComplete: false
            },
            triming: '',
            binding: '',
            booklets: '',
            discount: null,
            delivery: {
                address: {
                    name: ''
                }
            }
        },
        price: {
            discount: null,
            index: null,
            nbA3sheets: 0,
            papers: 0,
            paper: 0,
            colors: 0,
            color: 0,
            nbColors: 0,
            binding: 0,
            bindings: 0,
            triming: 0,
            fullTriming: 0,
            trimingCost: 0,
            doBooklets: 0,
            shipping: 0,
            total: 0
        },
        order: {
            archiveFiles: null,
            archiveUrl: null,
            //archiveUrl: 'http://clemsy.com/zip.zip',
            copies: 0,
            format: 0,
            sheets: 0,
            pages: 0,
            paper: {
                name: 0,
                price: 0,
            },
            colors: {
                type: {
                    standard: 0,
                    special: 0
                },
                price: {
                    standard: 0,
                    special: 0
                }

            },
            triming: 0,
            fullTriming: 0,
            binding:0,
            booklets: 0,
            shipping: "self",
            payment: null,
            grandTotal: 0
        }
    },
    mounted: function() {
        //console.log('created');

        this.getData();
        setTimeout(() => this.createPages());

        setTimeout(() => this.doQuotation());

    },

    watch: {
        'changes': function(val, oldVal){
            this.doQuotation();
            // alert(val)
        }
    },
    methods: {
        /*
          Submits the file to the server
        */


        submitFile: function(){

                let formData = new FormData();
                formData.append('file', this.file);
                axios.post( '/single-file',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(function(){
                    console.log('SUCCESS!!');
                })
                    .catch(function(){
                        console.log('FAILURE!!');
                    });

        },


        /*
          Handles a change on the file upload
        */
        handleFileUploads: function(){

            console.log(this.file)

            this.file = this.file.file[0];

            //alert('File Uploaded!')

        },
        makeid: function() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 24; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },
        sendOrder:function () {

            let timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
            let MD5 = function(d){result = M(V(Y(X(d),8*d.length)));return result.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}
            //let orderHash = MD5(timeStampInMs)

            let orderHash = this.makeid();

            console.log(orderHash)

            let urlOrderView = "/order/view.php?order="+orderHash
            // alert(urlOrderView)

            let orderData = {
                orderHash: orderHash,
                form: this.form,
                price: this.price,
                order: this.order,
                deliveryData: this.deliveryData,
            }

            // console.log(this.orderData);

            axios.post('./php/post.php', orderData, {headers: {
                    'Content-type': 'application/x-www-form-urlencoded',
                }}).then(
                    // r => console.log('r: ', JSON.stringify(r, null, 2));
                window.location.replace(urlOrderView)
            );

        },
        calculateTrimingCost: function () {
            // console.log('calculating triming cost')
            // console.log(this.config.extraCosts.triming.quantities)

            for (key = 0; key < this.config.extraCosts.triming.quantities.length; key++) {
                // console.log(this.config.extraCosts.triming.quantities[key])
                if(this.price.nbA3sheets<this.config.extraCosts.triming.quantities[key]) {
                   // this.trimingCost = this.config.extraCosts.triming.price
                }
            }
        },
        findPaperPrice: function() {
            setTimeout(function() {}, 500);

            let sheets = this.config.sheetsPerA3[this.form.format];
            let quantity = (this.form.quantity*this.form.totalPages)/sheets;
            this.order.pages = this.form.totalPages;
            // console.log('sheetsPerA3: '+ sheets)
            // console.log('quantity: '+ quantity)
            quantity = Math.ceil(quantity)
            this.price.nbA3sheets = quantity;
            let papers = this.config.papers[this.form.paper].quantities

            if(this.form.size!="A3") {
                this.triming = true;
            }

            for (key = 0; key < papers.length; key++) {
                if(quantity<=papers[key])
                {
                    this.price.paper = this.config.papers[this.form.paper].prices[key]
                    this.price.papers = quantity;
                    this.price.index = key;
                    // console.log("index: "+key)
                    // console.log("q: "+papers[key])
                    break;
                }
            }

            this.order.paper.name = this.form.paper;
            this.order.paper.price = this.price.paper //*this.order.pages;
        },
        findColorPrice: function () {

            let format = this.form.format;
            let copies = this.form.quantity;
            let nbSheets = this.form.formatIsReduced === true ? this.config.formats[format]['reduced'] : this.config.formats[format]['plain'] ;
            this.order.format = format;
            this.order.copies = copies;
            this.order.sheets = Math.ceil(copies/nbSheets);

            // pages :
            let pages = this.form.pages;

            let nbColors = {
                "standard": 0,
                "special": 0
            };

            for(key = 0; key <pages.length; key++) {
                // console.log('page : '+ key)
                let side = pages[key].colors

                for(ckey = 0; ckey<side.recto.length;ckey++)
                {
                    let color = side.recto[ckey];
                    // console.log('color : '+ color)
                    // console.log('price : '+ this.config.colors.standard[color].type)
                    nbColors[this.config.colors.standard[color].type]++;
                }
                // if artwork has verso
                if(pages[key].hasVerso===true) {

                    for(ckey = 0; ckey<side.verso.length;ckey++)
                    {
                        let color = side.verso[ckey];
                        // console.log('color : '+ color)
                        // console.log('price : '+ this.config.colors.standard[color].type)
                        nbColors[this.config.colors.standard[color].type]++;
                    }
                }
            }

            // nb colors :
            // console.log('nb std : ' + nbColors['standard'])
            // console.log('nb spe : ' + nbColors['special'])

            // price per color
            // console.log('index price color std : ' +  this.config.colors.price['standard'][this.price.index])
            // console.log('index price color spe : ' +  this.config.colors.price['special'][this.price.index])

            // price page

            this.order.colors.type.standard = nbColors['standard'];
            this.order.colors.type.special  = nbColors['special'];
            this.order.colors.price.standard = this.config.colors.price['standard'][this.price.index];
            this.order.colors.price.special  = this.config.colors.price['special'][this.price.index];

            // price /a3

            // full bleed triming
            if(this.form.formatIsReduced===true) {
                this.order.fullTriming=true;
            }else{
                this.order.fullTriming=false;
            }
            // triming
            if(this.form.format!=='A3') {
                this.order.triming=true;
            }else{
                this.order.triming=false;
            }

        },
        findPrintingPrice: function() {


            let pages = this.form.pages;
            this.price.nbColors = 0;

            let nbColors = {
                "std": 0,
                "spe": 0
            };

            for(key = 0; key <pages.length; key++) {
                // check si color est std ou spe
                this.price.nbColors += pages[key].colors.recto.length
                if(pages[key].hasVerso===true) {
                    this.price.nbColors += pages[key].colors.verso.length
                }
            }

            let colorPrices  = this.config.prices
            let stdPrice = this.config.colors.standard;
            let spePrice = this.config.colors.special;

            this.price.color = colorPrices[this.price.index]
            this.price.colors = this.price.color*this.price.nbColors*this.price.nbA3sheets;

            // console.log("total: "+price*nbCOlors*this.form.quantity)
            // console.log("index: "+this.price.index)
            // console.log("prix du passage: "+price)
        },
        getTotalPrice: function () {
            this.price.total = 0;
            this.price.total  = this.price.shipping;
            this.price.total += this.price.colors
            //  this.price.total += (this.price.paper*this.price.nbA3sheets*this.form.totalPages)
            // console.log(this.price.total)
        },
        applyDiscount: function () {
            //console.log(this.form.discount)
            if(this.form.discount == true) {
                this.price.discount = true
            } else{
                this.price.discount = null
            }
            this.doQuotation()
        },
        grandTotal: function () {
            let gt = 0;
            let gPapers  = this.order.paper.price * this.order.sheets;

            let gMasters = this.config.colors.master.price * (this.order.colors.type.standard + this.order.colors.type.special)

            // console.log('gPapers: '+ gPapers)
            let gStd   = this.order.colors.type.standard * this.order.colors.price.standard * this.order.sheets;
            // console.log('gRecto: '+ gStd)
            let gSpe   = this.order.colors.type.special * this.order.colors.price.special * this.order.sheets;
            // console.log('gVerso: '+ gSpe)
            let gTriming = this.order.triming === true ? this.config.extraCosts.triming.price : 0;
            // console.log('gTriming: '+ gTriming)
            let gFBTrim  = this.order.fullTriming === true ? this.config.extraCosts.triming.price : 0;
            // console.log('gFBTrim: '+ gFBTrim)


            let gShipping = this.order.shipping!='' ? this.config.delivery[this.order.shipping].price : ''; //this.config.delivery[this.order.shipping]['price'];

            let gBindings = this.order.binding >0 ? this.config.extraCosts.binding.price*this.order.binding : 0;

            let gBooklets = this.order.booklets >0 ? this.config.extraCosts.booklet.price*this.order.copies : 0;

            // console.log('delivery price: '+JSON.stringify(this.config.delivery[this.order.shipping].price))


            gt = gPapers+gMasters+gStd+gSpe+gTriming+gFBTrim+gBindings+gBooklets;


            if( this.price.discount != null ) {
                gt = gt/this.config.vat.ratio
            }

            gt += gShipping



            this.order.grandTotal = gt;
            // console.log('gt: '+ gt)

            // gt += this.order.colors.type.special * this.order.colors.price.special;
        },
        doQuotation: function() {

            this.findPaperPrice();
            this.findPrintingPrice();
            setTimeout(() => this.calculateTrimingCost());

            this.findColorPrice()

            this.getTotalPrice();
            this.grandTotal();

            this.form.totalPrice = '';
        },
        removePage: function(index) {
            console.log(index);
            if(this.form.totalPages>0) {
                if(confirm('Are you sure ?')) {

                    this.form.pages.splice(index,1);
                    this.doQuotation()
                }
            }
        },
        addPage: function() {
            this.form.totalPages++
            let page = JSON.parse(JSON.stringify(this.form.page));
            page.name = "Artwork " + this.form.totalPages
            this.form.pages.push(Vue.util.extend({}, page))
            //this.doQuotation()
        },

        createPages: function () {
            for (i = 1; i <= this.form.totalPages; i++) {
                let page = JSON.parse(JSON.stringify(this.form.page));
                page.name = "Artwork " + i
                this.form.pages.push(Vue.util.extend({}, page))
            }
            setTimeout(() => this.doQuotation())
        },

        prev: function() {
            this.form.step--;
        },
        next: function() {
            if(this.validateOrder()) {
                this.form.step++;
            }
        },
        checkVersoColors: function(pageId) {
            let hasVerso = this.form.pages[pageId].hasVerso;
            if(hasVerso===false) {
                this.form.pages[pageId].colors.verso = []
                this.form.pages[pageId].files.verso.url = null
            }

        },
        submit: function() {
            alert('Submit to blah and show blah and etc.');
        },

        validateOrder: function() {
            this.form.errors = null;

            if(this.form.paper=='')
            {
                this.form.errorMessages['paper'] = true;
                console.log('error paper : '+this.form.errorMessages['paper'])
                this.form.errors++
                // return false;
            }else{
                this.form.errorMessages['paper'] = false;
            }
            if(this.form.format=='')
            {
                this.form.errorMessages['format'] = true;
                console.log('error format : '+this.form.errorMessages['format'])
                this.form.errors++
                // return false;
            }else{
                this.form.errorMessages['format'] = false;
            }
            if(this.form.quantity<1)
            {
                this.form.errorMessages['quantity'] = true;
                console.log('error quantity : '+this.form.errorMessages['quantity'])
                this.form.errors++
                // return false;
            }else{
                this.form.errorMessages['quantity'] = false;
            }

            // Step 2
            if(this.form.step==2) {
                if(this.order.archiveUrl===null) {
                    this.form.errorMessages['archiveUrl'] = true;
                    this.form.errors++
                }else{
                    this.form.errorMessages['archiveUrl'] = false;
                }

                let col = this.order.colors.type.standard+this.order.colors.type.special
                if(col < this.form.totalPages) {
                    this.form.errorMessages['colors'] = true;
                    this.form.errors++
                }else{

                    this.form.errorMessages['colors'] = false;
                }

            }


            // step 3 delivery

            if(this.form.step==3) {

                if(this.deliveryData.nameFirst===null) {
                    this.form.errorMessages['first name'] = true;
                    this.form.errors++
                }else{
                    this.form.errorMessages['first name'] = false;
                }

                if(this.deliveryData.nameLast===null) {
                    this.form.errorMessages['last name'] = true;
                    this.form.errors++
                }else{
                    this.form.errorMessages['last name'] = false;
                }

                if(this.deliveryData.email===null) {
                    this.form.errorMessages['email'] = true;
                    this.form.errors++
                }else{
                    this.form.errorMessages['email'] = false;
                }

                if(this.deliveryData.phone===null) {
                    this.form.errorMessages['phone'] = true;
                    this.form.errors++
                }else{
                    this.form.errorMessages['phone'] = false;
                }

                if(this.deliveryData.geolocalized===null) {
                    this.form.errorMessages['delivery address'] = true;
                    this.form.errors++
                }else{
                    this.form.errorMessages['delivery address'] = false;
                }

            }

            console.log('errors :'+ this.form.errors)

            if(this.form.errors > 0) {
                return false
            }else{
                return true;
            }
        },
        onValidated: function(isValid, errors) {
            console.log("Validation result: ", isValid, ", Errors:", errors);
            if(!isValid) {

                this.form.errorMessages['address'] = false;
                this.form.errors++
                return false;
            }
        },
        getJsonFromArr: function(arr) {
            let index = 0;
            function request() {
                return axios.get(index).then(() => {
                    index++;
                if (index >= arr.length) {
                    return 'done'
                }
                return request();
            });

                console.log('loaded : ' + index)
            }
            return request();
        },

        getData: function() {

            var configFiles = [
                "./jsons/colors.json"
            ]

            // this.getJsonFromArr(configFiles);

            // https://github.com/axios/axios/issues/1413
            const self = this

            axios.get("./js/config.json")
                .then(response => {
                    self.config = response.data
                })
                .catch(e => {
                        self.errors.push(e);
                });





        },
        inArray: function(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        },
        previewImage: function(event) {
            // Reference to the DOM input element
            var input = event.target;
            var imgTarget = input.getAttribute("data-target");
            //console.log(imgTarget);
            // Ensure that you have a file before attempting to read it
            if (input.files && input.files[0]) {
                // create a new FileReader to read this image and convert to base64 format
                var reader = new FileReader();
                // Define a callback function to run, when FileReader finishes its job
                reader.onload = (e) => {
                    // Note: arrow function used here, so that "this.imageData" refers to the imageData of Vue component
                    // Read image as base64 and set to imageData
                    this.uploadedFile[imgTarget] = e.target.result;
                }
                // Start the reader job - read file as a data url (base64 format)
                reader.readAsDataURL(input.files[0]);
            }
        },
        toggleDiv: function (event) {
            let el = event.target.getAttribute("data-target");
            $('#'+el).toggle();
        },

        prettyJSON: function(json) {
            if (json) {
                json = JSON.stringify(json, undefined, 4);
                json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }
        }

    }
});

