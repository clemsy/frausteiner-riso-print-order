// import VueRescue from 'vue-rescue-input'
// var VueRescue = require('./vue-rescue-input')
// Vue.use(saveInput)
// Vue.use(VuexForms)


Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.use(VeeValidate);

const vm = new Vue({
    el: '#app',
    data: {
        config: [],
        uploadedFile: {
            'recto': '',
            'verso': ''
        },
        form:  {
            step: 1,
            type: '',
            size: 'A3',
            name: '',
            email: '',
            quantity:'',
            paper: '',
            totalPages: 1,
            pages: [],
            page: {
                name: '',
                files: {
                    recto: {
                        upload: '',
                        url: ''
                    },
                    verso: {
                        upload: '',
                        url: ''
                    }
                },
                colors: {
                    recto: [],
                    verso: []
                },
                hasVerso: false,
                isValidated: false
            },
            trim: '',
            binding: '',
            booklets: '',
            delivery: {
                address: {
                    name: ''
                }
            }
        }

    },
    created: function() {
        //console.log('created');
        this.getData();
        this.createPages();
    },
    methods: {

        createPages: function () {
            for (i = 1; i <= this.form.totalPages; i++) {
                let page = JSON.parse(JSON.stringify(this.form.page));
                page.name = "Page " + i
                this.form.pages.push(Vue.util.extend({}, page))
            }
        },
        prev: function () {
            this.form.step--;
        },
        next: function () {
            this.form.step++;
        },
        removePage: function() {
            this.form.pages = [];
            this.form.totalPages -= 1;
            this.createPages();
        },
        addPage: function() {
            this.form.pages = [];
            this.form.totalPages += 1;
            alert(this.form.totalPages)
            this.createPages();

        },
        submit: function () {
            alert('Submit to blah and show blah and etc.');
        },
        getData: function() {

            axios
                .get("./js/config.json")
                .then(
                    (response) => {
                        this.config = response.data;

                })
                .catch(
                        error => { console.log(error); }
                );
        },
        inArray: function(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        },

        getSearchParams: function(k){
            var p={};
            location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
            return k?p[k]:p;
        }

    }
});
