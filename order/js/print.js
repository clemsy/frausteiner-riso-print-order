// import VueRescue from 'vue-rescue-input'
// var VueRescue = require('./vue-rescue-input')
// Vue.use(saveInput)
// Vue.use(VuexForms)


Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.use(VeeValidate);

const vm = new Vue({
    el: '#app',
    data: {
        config: [],
        form:  {
            step: 2,
            type: '',
            size: 'A3',
            quantity: 1,
            paper: '',
            totalPages: 2,
            pages: [],
            page: {
                name: '',
                files: {
                    recto: {
                        upload: '',
                        url: ''
                    },
                    verso: {
                        upload: '',
                        url: ''
                    }
                },
                colors: {
                    recto: [],
                    verso: []
                },
                hasVerso: false,
                isComplete: false
            },
            trim: '',
            binding: '',
            booklets: '',
            delivery: {
                address: {
                    name: ''
                }
            }
        }

    },
    created: function() {
        console.log('created');
        this.getData();
        this.createPages();
    },
    methods: {

        removePage: function() {
            this.form.pages = [];
            this.form.totalPages -= 1;
            this.createPages();
        },
        addPage: function() {
            this.form.pages = [];
            this.form.totalPages += 1;
            this.createPages();

        },
        prev: function() {
            this.form.step--;
        },
        next: function() {
            this.form.step++;
        },
        submit: function() {
            alert('Submit to blah and show blah and etc.');
        },
        createPages: function () {
            for (i = 1; i <= this.form.totalPages; i++) {
                let page = JSON.parse(JSON.stringify(this.form.page));
                page.name = "Page " + i
                this.form.pages.push(Vue.util.extend({}, page))
            }
        },
        getData: function() {
            axios
                .get("./js/config.json")
                .then((response) => { this.config = response.data; })
                .catch(error => { console.log(error); });
        },
        inArray: function(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        },
        previewImage: function(event) {
            // Reference to the DOM input element
            var input = event.target;
            var imgTarget = input.getAttribute("data-target");
            //console.log(imgTarget);
            // Ensure that you have a file before attempting to read it
            if (input.files && input.files[0]) {
                // create a new FileReader to read this image and convert to base64 format
                var reader = new FileReader();
                // Define a callback function to run, when FileReader finishes its job
                reader.onload = (e) => {
                    // Note: arrow function used here, so that "this.imageData" refers to the imageData of Vue component
                    // Read image as base64 and set to imageData
                    this.uploadedFile[imgTarget] = e.target.result;
                }
                // Start the reader job - read file as a data url (base64 format)
                reader.readAsDataURL(input.files[0]);
            }
        }

    }
});


const modal = '';
function toggleModalClasses(event) {
    var modalId = event.currentTarget.dataset.modalId;
    const modal = $(modalId);
    modal.toggleClass('is-active');
    $('html').toggleClass('is-clipped');
};

$('.open-modal').click(toggleModalClasses);
$('.close-modal').click(toggleModalClasses);
