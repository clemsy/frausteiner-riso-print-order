const vm = new Vue({
    el: '#app',

    data: {
        config: [],
        order: [],
    },


    mounted: function() {

        let uri = window.location.search.substring(1);
        let params = new URLSearchParams(uri);
        let orderId = params.get("order");
        //console.log(orderId);

        this.getOrder(orderId);
        this.getConfig();

    },
    methods: {

        getConfig: function() {
            const self = this
            axios.get("./js/config.json")
                .then(response => {
                self.config = response.data
            })
            .catch(e => {
                    self.errors.push(e);
            });

        },
        getOrder: function(orderId) {

            axios.get("./orders/order-"+orderId+".json")
                .then(response => {
                this.order = response.data
            })
            .catch(e => {
                    this.errors.push(e);
            });
        },
    }
});