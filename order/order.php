<?php
$showNavigation = false;
include('./_header.php'); ?>

<?php
    $file = file_get_contents($_SERVER['DOCUMENT_ROOT']."/order/orders/order-".$_GET['order'].".json");
    $o = json_decode($file, true);
?>

<div class="container-fluid">
    <h1>Order #<?php echo $_GET['order']; ?></h1>

    <p>
        <strong>YAY! Thank you for printing with <a href="http://frausteiner.be/">Frau Steiner</a>!</strong>
    </p>
    <p>
        Your order has been placed successfully and will be processed shortly.
    </p>

    <p>
        <h3>Payment</h3>
        <?php if($o['order']['payment']== 'Paypal'): ?>
        Please click on <a href="https://paypal.me/frausteiner/<?= round($o['order']['grandTotal'], 2); ?>">that link</a> to pay with paypal.

        <?php elseif($o['order']['payment']== 'Bank Transfer'): ?>
        You choosed to pay via Bank Transfer.
        <br>
        Here are the bank informations
        <br>
        <strong>
        FRAU STEINER
            <br>
        BELFIUS BANK SA
            <br>
        BBAN: 068-9037316-47
            <br>
        IBAN: BE22 0689 0373 1647
            <br>
        BIC (SWIFT No): GKCCBEBB
            <br><br>
            Amount : <?= round($o['order']['grandTotal'], 2); ?>
            <br>
            Communication : Your Name + Project Name
        </strong>

        <?php endif; ?>
    </p>

    <!-- #app -->
    <div id="app" class="row">


        <!-- cart view -->
        <div class="col-12">
            <div class="bdtd" id="results">

                <div class="cart" style="padding:20px 40px">
                    <h2>Customer Details</h2>
                    <dl class="row">
                        <dt class="col-sm-2"><span>Name:</span></dt>
                        <dd class="col-sm-10"><?= $o['deliveryData']['nameFirst']; ?> <?= $o['deliveryData']['nameLast']; ?></dd>
                        <dt class="col-sm-2"><span>Address:</span></dt>
                        <dd class="col-sm-10"><?= $o['deliveryData']['geolocalized']; ?> </dd>
                        <dt class="col-sm-2"><span>Email:</span></dt>
                        <dd class="col-sm-10"><?= $o['deliveryData']['email']; ?> </dd>
                        <dt class="col-sm-2"><span>Phone:</span></dt>
                        <dd class="col-sm-10"><?= $o['deliveryData']['phone']; ?> </dd>
                        <hr>
                        <dt class="col-sm-2"><span>Message:</span></dt>
                        <dd class="col-sm-10">
                            <div style="white-space: pre;"><?= (isset($o['deliveryData']['comments']) ? $o['deliveryData']['comments'] : ''); ?></div>
                        </dd>
                    </dl>
                </div>

                <div class="cart" style="padding:0 40px 20px">
                    <h2>Payment and Delivery Details</h2>
                    <dl class="row">
                        <dt class="col-sm-2"><span>Grand Total:</span></dt>
                        <dd class="col-sm-10"><?= round($o['order']['grandTotal'], 2); ?> € TVAC</dd>
                        <dt class="col-sm-2"><span>Delivery:</span></dt>
                        <dd class="col-sm-10"><?= $o['order']['shipping']; ?> </dd>
                        <dt class="col-sm-2"><span>Payment:</span></dt>
                        <dd class="col-sm-10"><?= $o['order']['payment']; ?> </dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /cart view -->
        <div class="divider">&nbsp;</div>
        <!-- /main content right -->
        <div class="col-12">

            <!-- order view starts here -->
            <div class="bdtd">
                <div class="cart" style="padding:20px 40px">

                    <h2>Order Details</h2>

                    <dl class="row">
                        <dt class="col-sm-2">
                            Paper:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['paper']['name']; ?>
                        </dd>
                        <dt class="col-sm-2">
                            Format:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['format']; ?>
                        </dd>
                        <dt class="col-sm-2">
                            Quantity:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['copies']; ?>
                        </dd>
                        <dt class="col-sm-2">
                            Fulltrim:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['fullTriming'] === true ? 'Yes' : 'No'; ?>
                        </dd>
                        <dt class="col-sm-2">
                            Bidings:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['binding']; ?>
                        </dd>
                        <dt class="col-sm-2">
                            Booklets:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['booklets']; ?>
                        </dd>
                    </dl>

                    <h2>Artworks Details</h2>

                    <dl class="row">
                        <dt class="col-sm-2">
                            Artwork archive file:
                        </dt>
                        <dd class="col-sm-10">
                            <a :href="order.order.archiveUrl" target="_blank">
                                <?= $o['order']['archiveUrl']; ?>
                            </a>
                        </dd>
                        <dt class="col-sm-2">
                            Number of artworks:
                        </dt>
                        <dd class="col-sm-10">
                            <?= $o['order']['pages']; ?>
                        </dd>

                    </dl>


                    <h2>Pages Details</h2>

                    <?php foreach ($o['form']['pages'] as $k => $page): ?>


                    <dl class="row">
                        <dt class="col-sm-2">
                            Page Name:
                        </dt>
                        <dd class="col-sm-10">
                            <?=$page['name'];?>
                        </dd>
                        <dt class="col-sm-2">
                            Colors
                        </dt>
                        <dd class="col-sm-10">
                            <ul v-for="(colors, side) in page.colors">
                                <?php foreach ($page['colors'] as $side => $colors): ?>
                                <li>
                                    <span><?=$side;?></span>
                                    <ol>
                                        <?php foreach ($colors as $i => $color): ?>
                                        <li v-for="color in colors">
                                            <?=$color;?>
                                            <br>
                                        </li>
                                        <?php endforeach; ?>
                                    </ol>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </dd>
                    </dl>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <!-- /main  -->


    </div>
    <!-- / #app -->

</div>

<hr>





<div class="modal" tabindex="-1" role="dialog" id="remoteContentModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>



<!-- Required scripts -->

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</script>



<script type="text/javascript">
    $(document).ready(function(){

        $('[data-load-remote]').on('click',function(e) {
            e.preventDefault();
            var $this = $(this);
            var remote = $this.data('load-remote');
            if(remote) {
                $($this.data('remote-target')).load(remote);
            }
        });

        $(document.body).on('hidden.bs.modal', function () {
            $("#changes").attr('value', $("#changes").attr('value')+1);
            console.log('modal hidden')
        })

        // $( "#openSpecial" ).trigger( "click" );

        $('a.btnModalRemote').on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $(".modal-body").html('<iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true" src="'+url+'"></iframe>');
        });

    });


</script>

</body>
</html>