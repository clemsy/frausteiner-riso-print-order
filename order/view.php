<?php include('./_header.php'); ?>


<div class="container-fluid">
    <h1>Order #<?php echo $_GET['order']; ?></h1>

    <!-- #app -->
    <div id="app" class="row">


        <!-- cart view -->
        <div class="col-5 col-xs-12">
            <div class="bdtd" id="results">

                <div class="cart" style="padding:20px 40px">
                <h2>Customer Details</h2>
                    <dl class="row">
                        <dt class="col-sm-2"><span>Name:</span></dt>
                        <dd class="col-sm-10">{{ order.deliveryData.nameFirst}} {{ order.deliveryData.nameLast}} </dd>
                        <dt class="col-sm-2"><span>Address:</span></dt>
                        <dd class="col-sm-10">{{ order.deliveryData.geolocalized}} </dd>
                        <dt class="col-sm-2"><span>Email:</span></dt>
                        <dd class="col-sm-10">{{ order.deliveryData.email}} </dd>
                        <dt class="col-sm-2"><span>Phone:</span></dt>
                        <dd class="col-sm-10">{{ order.deliveryData.phone}} </dd>
                        <hr>
                        <dt class="col-sm-2"><span>Message:</span></dt>
                        <dd class="col-sm-10">
                            <div style="white-space: pre;">{{ order.deliveryData.comments|nl2br}}</div>
                        </dd>
                    </dl>
                </div>

                <div class="cart" style="padding:0 40px 20px">
                    <h2>Payment and Delivery Details</h2>
                    <dl class="row">
                        <dt class="col-sm-2"><span>Grand Total:</span></dt>
                        <dd class="col-sm-10">{{ order.order.grandTotal| currency('€ ')}} </dd>
                        <dt class="col-sm-2"><span>Delivery:</span></dt>
                        <dd class="col-sm-10">{{ order.order.shipping}} </dd>
                        <dt class="col-sm-2"><span>Payment:</span></dt>
                        <dd class="col-sm-10">{{ order.order.payment}} </dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /cart view -->

        <!-- /main content right -->
        <div class="col-7 col-xs-12">

            <!-- order view starts here -->
            <div class="bdtd">
                <div class="cart" style="padding:20px 40px">

                    <h2>Order Details</h2>

                    <dl class="row">
                        <dt class="col-sm-2">
                            Paper:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.paper.name }}
                        </dd>
                        <dt class="col-sm-2">
                            Format:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.format }}
                        </dd>
                        <dt class="col-sm-2">
                            Quantity:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.copies }}
                        </dd>
                        <dt class="col-sm-2">
                            Fulltrim:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.fullTriming }}
                        </dd>
                        <dt class="col-sm-2">
                            Bidings:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.binding }}
                        </dd>
                        <dt class="col-sm-2">
                            Booklets:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.booklets }}
                        </dd>
                    </dl>

                    <h2>Artworks Details</h2>

                    <dl class="row">
                        <dt class="col-sm-2">
                            Artwork archive file:
                        </dt>
                        <dd class="col-sm-10">
                            <a :href="order.order.archiveUrl" target="_blank">
                                {{ order.order.archiveUrl }}
                            </a>
                        </dd>
                        <dt class="col-sm-2">
                            Number of artworks:
                        </dt>
                        <dd class="col-sm-10">
                            {{ order.order.pages }}
                        </dd>

                    </dl>


                    <h2>Pages Details</h2>

                    <dl class="row"  v-for="(page, index) in order.form.pages">
                            <dt class="col-sm-2">
                                Page Name:
                            </dt>
                            <dd class="col-sm-10">
                                {{ page.name}}
                            </dd>
                            <dt class="col-sm-2">
                                Colors
                            </dt>
                            <dd class="col-sm-10">
                                <ul v-for="(colors, side) in page.colors">

                                    <li>
                                        <span>{{side}}</span>
                                        <ol>
                                            <li v-for="color in colors">
                                                {{ color }}
                                                <br>
                                            </li>
                                        </ol>
                                    </li>
                                </ul>
                            </dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /main  -->


    </div>
    <!-- / #app -->

</div>

<hr>
<div class="modal" tabindex="-1" role="dialog" id="remoteContentModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>



<!-- Required scripts -->

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vee-validate@2.0.0-rc.18/dist/vee-validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue2-filters/dist/vue2-filters.min.js"></script>
<script src="https://unpkg.com/vue-form-generator@2.2.2/dist/vfg.js"></script>
<script src="https://unpkg.com/vue-cookies@1.5.5/vue-cookies.js"></script>
<script src="https://unpkg.com/vue-router"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsZbRNQabdrTr3mOVGGVgol9JWzUnoqyw&libraries=places">
</script>
<script src="./js/view.js"></script>



<script type="text/javascript">
    $(document).ready(function(){

        $('[data-load-remote]').on('click',function(e) {
            e.preventDefault();
            var $this = $(this);
            var remote = $this.data('load-remote');
            if(remote) {
                $($this.data('remote-target')).load(remote);
            }
        });

        $(document.body).on('hidden.bs.modal', function () {
            $("#changes").attr('value', $("#changes").attr('value')+1);
            console.log('modal hidden')
        })

        // $( "#openSpecial" ).trigger( "click" );

        $('a.btnModalRemote').on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $(".modal-body").html('<iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true" src="'+url+'"></iframe>');
        });

    });


</script>

</body>
</html>
