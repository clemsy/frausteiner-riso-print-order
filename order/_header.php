<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Order your RISO Prints - Frau Steiner</title>

    <!-- Required Stylesheets --><!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//unpkg.com/vue-form-generator@2.0.0/dist/vfg.css">
    <link rel="stylesheet" href="./css/custom.css">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light mb-3 pb-1" style="background-color: #fff;border-bottom: 1px solid #F5F5F5;">
    <a class="navbar-brand" href="/">frau steiner</a>

    <?php if (!isset ($showNavigation) ): ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse  justify-content-between" id="navbarSupportedContent">
        <div class="navbar-nav"></div>
        <div class="navbar-nav">

        <ul class="navbar-nav mr-auto">

            <li class="nav-item">
                <a class="nav-link" href="#special-offers" style="">* Special offers *</span></a>
            </li>
            <li class="nav-item nav-link-bd">
                <a class="nav-link" href="/calendar-2019/">Calendar 2019</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/workshops/">Workshops</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/order/">Order your prints</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/filesetup/">File setup</a>
            </li>

            <?php /*
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    File setup
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="./guides/papers.html" data-remote-target="#remoteContentModal .modal-body">papers</a>
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="./guides/formats.html" data-remote-target="#remoteContentModal .modal-body">formats</a>
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="./guides/colors.html" data-remote-target="#remoteContentModal .modal-body">colors</a>
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="./guides/files.html" data-remote-target="#remoteContentModal .modal-body">prepare your files</a>

                </div>
            </li>
 */ ?>
            <li class="nav-item">
                <a class="nav-link" href="/contact/">Contact</span></a>
            </li>

        </ul>
        </div>
    </div>
    <?php endif;?>
</nav>

<div class="contenair-fluid mb-5 collapse " style="border-top: 1px solid #E2E2E2">
    <img src="/order/img/FS-OrderYourPrints.png" style="width: 100%" />
</div>