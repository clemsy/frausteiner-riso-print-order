<div class="col-5 col-xs-hidden d-sm-none d-md-block">

    <h3 class=""></h3>
    <div class="bdtd" id="results" v-if="form.types!=''">

        <div class="cart" style="padding:20px 40px">


            <div  class=" " style="padding:20px 0">

                <dl class="row">

                    <dt class="col-sm-6"><h3>Paper:</h3></dt>
                    <dd class="col-sm-4"></dd>


                    <dt class="col-sm-10"><h5>{{form.paper }}</h5></dt>


                    <dt class="col-sm-6 text-muted">Paper A3 Sheet Unit Price:</dt>
                    <dd class="col-sm-4 text-muted">{{order.paper.price| currency('€ ') }}</dd>

                    <dt class="col-sm-6 text-muted">Quantity of A3 Sheets:</dt>
                    <dd class="col-sm-4 text-muted">{{ order.sheets*order.pages }}</dd>

                    <dt class="col-sm-6">Paper's total price :</dt>
                    <dd class="col-sm-4">{{ order.paper.price*order.sheets | currency('€ ') }}</dd>


                    <dt class="col-sm-8 mt-2"><h3>Colors:</h3></dt>
                    <dd class="col-sm-2"></dd>

                    <dt class="col-sm-6 text-muted">Standard Color Unit Price:</dt>
                    <dd class="col-sm-4 text-muted">{{order.colors.price.standard| currency('€ ') }}</dd>

                    <dt class="col-sm-6 text-muted">Qty of std Colors:</dt>
                    <dd class="col-sm-4 text-muted">{{order.colors.type.standard }}</dd>

                    <dt class="col-sm-6 text-muted">Standard Colors:</dt>
                    <dd class="col-sm-4 text-muted">{{order.colors.type.standard * order.colors.price.standard| currency('€ ') }}</dd>

                    <dt v-if="order.colors.type.special>0" class="col-sm-6 text-muted">Special Color Unit Price:</dt>
                    <dd v-if="order.colors.type.special>0" class="col-sm-4 text-muted">{{order.colors.price.special| currency('€ ') }}</dd>

                    <dt v-if="order.colors.type.special>0" class="col-sm-6 text-muted">Qty of special Colors:</dt>
                    <dd v-if="order.colors.type.special>0" class="col-sm-4 text-muted">{{order.colors.type.special }}</dd>

                    <dt v-if="order.colors.type.special>0" class="col-sm-6 text-muted">Special Colors:</dt>
                    <dd v-if="order.colors.type.special>0" class="col-sm-4 text-muted">{{order.colors.type.special * order.colors.price.special| currency('€ ') }}</dd>



                    <dt class="col-sm-6 text-muted">Qty of masters:</dt>
                    <dd class="col-sm-4 text-muted">{{order.colors.type.standard+order.colors.type.special }}</dd>

                    <dt class="col-sm-6 text-muted">Masters's Price:</dt>
                    <dd class="col-sm-4 text-muted">{{ (order.colors.type.standard+order.colors.type.special)*config.colors.master.price | currency('€ ') }}</dd>


                    <dt class="col-sm-6">Printing Costs:</h3></dt>
                    <dd class="col-sm-4">{{order.sheets*(order.colors.type.standard * order.colors.price.standard) + (order.colors.type.special * order.colors.price.special) + ((order.colors.type.standard+order.colors.type.special)*config.colors.master.price)  | currency('€ ') }}</dd>



                    <dt class="col-sm-8 mt-2"><h3>Extra Printing Costs:</h3></dt>
                    <dd class="col-sm-2"></dd>

                    <dt class="col-sm-6" v-if="order.binding>0">Binding:</dt>
                    <dd class="col-sm-4" v-if="order.binding>0">{{config.extraCosts.booklet.price*order.binding*(order.copies/50)| currency('€ ') }}</dd>

                    <dt class="col-sm-6" v-if="order.booklets>0">Booklets:</dt>
                    <dd class="col-sm-4" v-if="order.booklets>0">{{config.extraCosts.booklet.price*(order.copies/50)| currency('€ ') }}</dd>

                    <dt class="col-sm-6" v-if="order.triming===true">Triming:</dt>
                    <dd class="col-sm-4" v-if="order.triming===true">{{config.extraCosts.triming.price| currency('€ ') }}</dd>

                    <dt class="col-sm-6" v-if="order.fullTriming===true">Full Bleed Triming:</dt>
                    <dd class="col-sm-4" v-if="order.fullTriming===true">{{config.extraCosts.triming.price| currency('€ ') }}</dd>

                    <dt class="col-sm-6" v-if="order.shipping!=0">Shipping:</dt>
                    <dd class="col-sm-4" v-if="order.shipping!=0">{{config.delivery[order.shipping].price| currency('€ ') }}</dd>

                </dl>


                <h5 class="text-muted">
                    Total HTVA (w/o shipping):
                    {{ (order.grandTotal-config.delivery[order.shipping].price)/1.21 | currency('€ ') }}

                </h5>

                <h3 class="">
                    Total TVAC:
                    {{ order.grandTotal| currency('€ ') }}

                </h3>
            </div>


        </div>

    </div>
    <div>
        <hr>
    </div>
    <div class="bdtd collapse ">


            <hr>

            <button class="btn magenta" v-on:click="doQuotation">update total</button>

            <button href="#" class="btn magenta" @click.prevent="toggleDiv($event)" data-target="priceDebug">show Price debug</button>
        <hr>
        order:
            <pre v-if="form"  class="collapse" id="priceDebug" v-html="prettyJSON(order)"></pre>
        <hr>
        price:
            <pre v-if="form"  class="collapse" id="priceDebug" v-html="prettyJSON(price)"></pre>

            <hr>
            <div class="row">&nbsp;</div>

            <a href="#" class="" @click.prevent="toggleDiv($event)" data-target="deliveryData">show delivery data debug</a>
            <pre v-if="form"  class="collapse" id="deliveryData" v-html="prettyJSON(deliveryData)"></pre>

            <hr>
            <a href="#" class="" @click.prevent="toggleDiv($event)" data-target="debugForm">show form debug</a>
            <pre v-if="form"  class="collapse" id="debugForm" v-html="prettyJSON(form)"></pre>
            <hr>
            <a href="#" class="" @click.prevent="toggleDiv($event)" data-target="debugConfig">show config debug</a>
            <pre class="collapse" id="debugConfig">{{ config }}</pre>

    </div>
</div>