
<fieldset>
<!--
    <div class="form-group">

        <label class="label">Artwork files upload</label>
        <input type="file" id="file" ref="file" v-on:change="handleFileUploads()" class="form-control form-control-lg">
    </div>


-->

    <div class="form-group">

        <label class="label">Artwork files URL</label>
        <input
                id="archiveUrl"
                type="text"
                class="form-control form-control-lg"
                placeholder="URL of Artworks Archive"
                v-model="order.archiveUrl"
                >
        <p id="archiveUrlHelp" class="form-text text-muted">
            Address where you uploaded the archive file.
            <br>
            Ex: http://www.dropbox.com/myfiles/u/208/MyArtwrokFiles.zip
            <br>
            <br>
            Use your favorite file sharing application (Dropbox, WeTransfer, ...)
            <br>

            <a href="//www.hongkiat.com/blog/15-great-free-online-file-sharing-alternatives/">Some file sharing providers</a>



        </p>
    </div>

</fieldset>