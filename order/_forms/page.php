
<div class="modal" tabindex="-1" role="dialog" :id="'modalPage'+index">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Page: {{page.name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    <label class="label">Name:</label>
                    <input
                            :id="'pageName'+index"
                            type="text"
                            class="form-control form-control-lg"
                            placeholder="Page Name"
                            v-model="page.name"
                            :name="'pages['+index+'][name]'">
                    <p :id="'pageName'+index+'Help'" class="form-text text-muted">FYI and communication only, page name will not be printed.</p>
                </div>

                <hr>

                <div class="form-group">
                    <label class="label">Sides:</label>
                    <br>
                    <label class="">
                        <input
                                :id="'pageSides'+index"
                                type="checkbox"
                                class="checkbox"
                                placeholder="verso"
                                v-model="page.hasVerso"
                                :name="'pages['+index+'][verso]'"
                                v-on:change="checkVersoColors(index)">
                        page has verso?
                    </label>
                    <p :id="'pageSides'+index+'Help'" class="form-text text-muted">Check this if your page has 2 sides (ie: your page has a recto and a verso).</p>
                </div>
                <hr>


                <!--
                <label class="label">Files:</label>

                <div class="form-group">
                    <div v-for="(side, b) in config.pageNames" v-if="side=='recto' || (side=='verso' && page.hasVerso==1)">
                        <label class="label">{{side|capitalize}}</label>

                        <input
                                :id="'pageFiles'+index+index"
                                placeholder="http://www.somedownloadurl.com/MyPageArchive.zip"
                                class="form-control form-control-lg"
                                v-model="page.files[side].url"
                                :name="'page.files[\''+side+'\'].url'">
                        <p :id="'pageFiles'+index+index+'Help'" class="form-text text-muted">Please enter page data URL (check print guide and how to send us your files).</p>
                    </div>

                </div>
                <hr>
                -->
                <div class="form-group">
                    <label class="label">Colors:</label>
                    <p id="pageColorsHelp" class="clearfix form-text text-muted">6 colors max per page side</p>
                </div>

                <div class="row">
                    <div class="col-6" v-for="(side, b) in config.pageNames" v-if="side=='recto' || (side=='verso' && page.hasVerso==1)">
                        <label class="label">{{side|capitalize}}</label>
                        <div class="control">
                            <ul class="list-unstyled">
                                <li v-for="(colorData, color) in config.colors.standard" v-if="colorData.availability===true">
                                    <input
                                            :id="'color'+color+index+side"
                                            v-model="page.colors[side]"
                                            type="checkbox"
                                            :name="'page.colors[\''+side+'\']'"
                                            :disabled="page.colors[side].length >= config.colors.maxPerSide && page.colors[side].indexOf(color) == -1"
                                            :value="color" >

                                    <label
                                            :for="'color'+color+index+side"
                                            class="checkbox colorLabel"
                                            :style="'color:'+colorData.hex+';background-color:'+colorData.hex+''">
                                        {{ color }}
                                    </label>
                                    <label
                                            :for="'color'+color+index+side"
                                            class="checkbox colorLabel">
                                    {{ color }}</label>
                                </li>

                                <li v-for="(colorData, color) in config.colors.special" v-if="colorData.availability===true">
                                    <input
                                            :id="'color'+color+page+side"
                                            v-model="page.colors[side]"
                                            type="checkbox"
                                            :name="'page.colors[\''+side+'\']'"
                                            :disabled="page.colors[side].length >= config.colors.maxPerSide && page.colors[side].indexOf(color) == -1"
                                            :value="color" >

                                    <label
                                            :for="'color'+color+page+side"
                                            class="checkbox colorLabel"
                                            :style="'color:'+colorData.hex+';background-color:'+colorData.hex+''">
                                        {{ color }}
                                    </label>
                                    <label
                                            :for="'color'+color"
                                            class="checkbox colorLabel">
                                        {{ color }}</label>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn magenta" data-dismiss="modal" v-on:click="removePage(index)">Delete Artwork</button>

                <button type="button" class="btn magenta" data-dismiss="modal" v-on:click="doQuotation">Save Artwork Data</button>


            </div>
        </div>
    </div>
</div>