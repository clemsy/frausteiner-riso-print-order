
<fieldset>
    <hr class="sm">
    <div class="form-group">
        <label>Discounts</label>
        <br>
        <label class="label">
            <input
                    type="checkbox"
                    class="checkbox"
                    v-model="form.discount"
                    v-on:change="applyDiscount"
                    value="15"
            >        <small>I am a student, please apply my discount</small>
        </label>
    </div>
    <hr>
    <div class="form-group">
        <label>Delivery</label>
        <br>
        <select
                class="form-control "
                id="formShipping"
                name="shipping"
                v-model="order.shipping"
                v-on:change="doQuotation">
            <option disabled>Choose a shipping option</option>
            <option v-for="(option, key, index) in config.delivery" :value="key">
                {{option.text}} - price : {{option.price | currency('€ ') }}
            </option>
        </select>
    </div>

</fieldset>

<fieldset>
    <hr class="sm">
    <div class="form-group">

        <label>Payment</label>

    </div>


    <div class="row">

        <div class="col-sm" style="">
            <h5>Bank Transfer</h5>
            <div class="float-left col-2 pt-5">
                <input
                        type="radio"
                        class="checkbox"
                        v-model="order.payment"
                        id="paymentBank"
                        checked="checked"
                        value="Bank Transfer">
            </div>
            <label class="float-left col-8" for="paymentBank">
                <p class="small">
                    Your order will be processed as soon as we get a payment confirmation.
                    <br>
                    Bank account number and all info will be sent in the confirmation email of your order!
                </p>
            </label>
        </div>

        <div class="col-sm">
            <h5>Paypal</h5>
            <div class="float-left col-2 pt-5">
                <input
                        type="radio"
                        class="checkbox"
                        v-model="order.payment"
                        id="paymentPaypal"
                        value="Paypal">
            </div>
            <label class="float-left col-8" for="paymentPaypal">
                <p class="small">
                    Please check your mailbox, and click on the link we will send you in a minute in the confirmation email of your order!
                </p>
            </label>
        </div>


    </div>

    <hr>
    <div class="form-group">


        <div class=" float-right ml-1 mt-5">
            <button type="submit" class="btn magenta ml-10" @click.prevent="sendOrder()">Send Order</button>
        </div>
        <div class=" float-right mt-5">
            <button class="btn magenta text-right" @click.prevent="prev()" v-if="form.step>1">&larr; Previous</button>
        </div>
    </div>
</fieldset>