
<hr class="sm">
<div class="form-group">
    <label>Remises</label>
    <br>
    <label class="label">
        <input
                type="checkbox"
                class="checkbox"
                v-model="form.discount"
                v-on:change="applyDiscount"
                value="15"
        >        <small>Je suis étudiant, je bénéficie des 15% de remise</small>
    </label>
</div>
<hr>
<div class="form-group">
    <label>Livraison</label>
    <br>
    <select
            class="form-control "
            id="formShipping"
            name="shipping"
            v-model="order.shipping"
            v-on:change="doQuotation">
        <option disabled>Choose a shipping option</option>
        <option v-for="(option, key, index) in config.delivery" :value="key">
            {{option.text}} - price : {{option.price | currency('€ ') }}
        </option>
    </select>
</div>
<!--
<div>
    <h3>Total:</h3>
    <span>{{ price.total | currency('€ ') }} EUR TVAC</span>
</div>
-->