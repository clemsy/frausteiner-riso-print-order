
<fieldset>

<div class="form-group">
    <label for="formPaper">Paper</label>
    <select
            class="form-control "
            id="formPaper"
            name="paper"
            v-model="form.paper"
            v-on:change="doQuotation">
        <option disabled selected>Choose a paper</option>
        <option v-for="(i, paper) in config.papers" v-if="i.availability===true"> {{paper}}</option>
        <!--
        <optgroup v-for="(d, paperGroup) in config.papers" :label="paperGroup">
            <option v-for="(i, paper) in d">{{ paperGroup }} - {{paper}}</option>
        </optgroup>
        -->
    </select>

    <p class="help">
        Choose your paper,
        <a href="#remoteContentModal" data-toggle="modal" data-load-remote="http://frausteiner.be/filesetup/#paper" data-remote-target="#remoteContentModal .modal-body">click here</a>
        for more details about the paper we have in stock.</p>
</div>



<div class="form-group">
    <label class="label">Format</label>
        <div class="select is-fullwidth">
            <select
                    class="form-control "
                    name="format"
                    v-model="form.format"
                    v-on:change="doQuotation">
                <option disabled>Choose a format</option>
                <option v-for="(format, formatKey) in config.formats" v-if="format.isAvailable===true">
                    {{formatKey}}
                </option>
            </select>
        </div>
    <p class="help">
        <a href="#remoteContentModal" data-toggle="modal" data-load-remote="http://frausteiner.be/filesetup/#format" data-remote-target="#remoteContentModal .modal-body">click here</a>
        for more details about formats.
    </p>
</div>

    <div class="form-group">
        <label class="label ">
            <input
                    type="checkbox"
                    class="checkbox"
                    placeholder="reduced"
                    v-model="form.formatIsReduced"
                    v-on:change="doQuotation"
                    >
            <small >Full format printed</small>
        </label>
        <p class="help">
            Check the box if you want to that your artwork is full printed (ie: no marges)
            <br>
            This will add an extra cost for full bleed trimming and make the number of A3 sheets increase.

        </p>
    </div>

<div class="form-group">
    <label class="label">Number of Copies</label>
    <input
            id="numberOfPrints"
            name="quantity"
            v-model="form.quantity"
            class="form-control "
            type="number"
            placeholder="Min: 1, max: 10.000"
            min="1" max="10000"
            v-on:change="doQuotation">
    <p id="numberOfPrintsHelp" class="form-text text-muted">Number of copies, minimum 1, maximum 10.000</p>
</div>


<!--
    <div class="form-group">
        <label class="label">Number of Bindings</label>
        <input
                id="numberOfBindings"
                name="binding"
                v-model="order.binding"
                class="form-control"
                type="number"
                placeholder="Min: 0, max: 2"
                min="0" max="2"
                v-on:change="doQuotation">
        <p id="numberOfBindingsHelp" class="form-text text-muted">Do you need that each sheet is binded ?
            <br>
            Cost per binding for 50 sheets is 5.00€</p>
    </div>
    <hr>
    <div class="form-group">
        <label class="label">Booklets</label>
        <br>
        <label class="">
            <input
                    id="booklets"
                    type="checkbox"
                    class="checkbox"
                    placeholder="booklets"
                    v-model="order.booklets"
                    name="'form.booklets"
                    v-on:change="doQuotation">
            <small>Yes please, do booklets</small>
        </label>
        <p id="bookletHelp" class="form-text text-muted">Booklet Help Text
            <br>
            Cost per booklet for 50 sheets is 2.00€</p>

    </div>
-->
</fieldset>