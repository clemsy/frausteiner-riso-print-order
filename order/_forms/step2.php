


<div class="form-group">

    <label class="label">Artwork files URL</label>
    <input
            id="archiveUrl"
            type="text"
            class="form-control form-control-lg"
            placeholder="URL of Artworks Archive"
            v-model="order.archiveUrl"
    >
    <p id="archiveUrlHelp" class="form-text text-muted">
        Address where you uploaded the archive file.
        <br>
        Ex: http://www.dropbox.com/myfiles/u/208/MyArtwrokFiles.zip
        <br>
        <br>
        If you don’t have any server, we recommend you to use www.wetransfer.com.
        <!--
        <br>

        <a href="//www.hongkiat.com/blog/15-great-free-online-file-sharing-alternatives/">Some file sharing providers</a>
        -->


    </p>
</div>

<label class="label">Number of Sheets</label>
<div class="form-inline">
    <div class="form-group mb-3">
        <input
                v-bind:min="1"
                v-bind:max="64"
                name="totalPages"
                v-model="form.pages.length > 0 ? form.pages.length : form.totalPages"
                class="form-control"
                type="text"
                placeholder="Number of pages">&nbsp;
        <button type="button" class="btn btn-light magenta" v-on:click="addPage">+</button>
    </div>
</div>


<div class=" clearfix">
    <p class="help">Enter the number total of sheets (ie: differents pages to print), minimum 1, maximum 64.</p>
</div>

<div class="clearfix">
    <label class="label">Sheets Colors and Files</label>
</div>

<!--
<div class="form-group">
    <label class="label">
        <small>Add a color to all artworks
        </small>
    </label>
    <select>
        <option>Choose</option>
        <option>black</option>
        <option>red</option>
        <option>yellow</option>
        <option>orange</option>
    </select>
</div>
-->

<div v-for="(page, index) in form.pages" class="card float-left mr-1 ml-2 mt-1 mb-2" style="width: 19rem">

    <div class="card-body">

        <h5 class="card-title">{{index+1}}:  {{page.name}}</h5>

        <h6 class="card-subtitle mb-2 text-muted">Colors</h6>


        <div v-for="(side, b) in config.pageNames" v-if="side=='recto' || (side=='verso' )">

            <span v-if="page.colors[side].length>0">{{side}} :</span>

            <span v-for="(color, index) in page.colors[side]">
                <!-- <pre>{{ config.colors.standard[color]["hex"] }}</pre>-->
                <span class="" :style="'color:'+config.colors.standard[color]['hex']">{{color}}</span>  <span v-if="index+1 < page.colors[side].length">, </span>
            </span>
        </div>

        <hr class="sm">
        <a href="#" class="btn btn-sm  magenta" v-on:click="removePage(index)">Delete</a> <!-- -->
        <a href="#" class="btn btn-sm  magenta" data-toggle="modal" :data-target="'#modalPage'+index">Edit</a> <!-- btn-block -->

        <!-- Modal -->
        <?php include('./_forms/page.php'); ?>
        <!-- /modal -->
    </div>
</div>