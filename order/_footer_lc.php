
<div class="modal" tabindex="-1" role="dialog" id="remoteContentModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>



<!-- Required scripts -->

<script src="//cdn.clm/js/jquery-3.3.1.min.js"></script>
<script src="//cdn.clm/js/popper.min.js"></script>
<script src="//cdn.clm/js/bootstrap.min.js"></script>
<script src="//cdn.clm/js/vue.js"></script>
<script src="//cdn.clm/js/axios.min.js"></script>
<script src="//cdn.clm/js/vee-validate.js"></script>
<script src="//cdn.clm/js/vue2-filters.min.js"></script>
<script src="//cdn.clm/js/vfg.js"></script>
<script src="//cdn.clm/js/vue-cookies.js"></script>

<script async defer
        src="//cdn.clm/js/gmap.js?key=AIzaSyDsZbRNQabdrTr3mOVGGVgol9JWzUnoqyw&libraries=places">
</script>
<script src="./js/order.js"></script>



<script type="text/javascript">
    $(document).ready(function(){

        $('[data-load-remote]').on('click',function(e) {
            e.preventDefault();
            var $this = $(this);
            var remote = $this.data('load-remote');
            if(remote) {
                $($this.data('remote-target')).load(remote);
            }
        });

        $(document.body).on('hidden.bs.modal', function () {
            $("#changes").attr('value', $("#changes").attr('value')+1);
            console.log('modal hidden')
        })

        // $( "#openSpecial" ).trigger( "click" );

    });


</script>

</body>
</html>