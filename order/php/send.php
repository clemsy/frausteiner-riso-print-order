<?php

use PHPMailer\PHPMailer\PHPMailer;
require 'vendor/autoload.php';


/**
 * Order Data JSON
 */


$file = file_get_contents($_SERVER['DOCUMENT_ROOT']."/order/orders/order-".$_GET['order'].".json");
$o = json_decode($file, true);

$customer['email'] = $o['deliveryData']['email'];
$customer['fullname'] = $o['deliveryData']['firstname'] . ' ' . $o['deliveryData']['lasname'];

/**
 * Config
 *
 */

$url_html_email = 'http://'.$_SERVER['HTTP_HOST']."/order/order.php?order=".$_GET['order'];

$config = array();

// Print@frausteiner.be 4@oN66nt

$config['mail']['Host'] = "mignon.metanet.ch";
$config['mail']['SMTPSecure'] = "tls";
$config['mail']['Port'] = 587;
$config['mail']['SMTPDebug'] = 0;
$config['mail']['Username'] = "print@frausteiner.be";
$config['mail']['Password'] = "4@oN66nt";

$config['mail']['addressAdmin'] = "print@frausteiner.be";
$config['mail']['nameAdmin'] = "Admin Frau Steiner Order App ADMIN";

$config['mail']['addressDev'] = "clemsy@gmail.com";
$config['mail']['nameDev'] = "Clément Chartier - DEV FRAU STEINER ORDER";

$config['mail']['addressCustomer'] = $customer['email'];
$config['mail']['nameCustomer'] = $customer['fullname'];

$config['mail']['addressOwner'] = "print@frausteiner.be";
$config['mail']['nameOwner'] = "Frau Steiner";

$config['mail']['SubjectCustomer'] = "Your order at Frau Steiner Riso Prints";
$config['mail']['SubjectOwner'] = "[Frau Steiner] New Order from website";

/**
 * Config
 */

$mail = new PHPMailer;
$mail->isSMTP();

//$mail->isSMTP();
$mail->SMTPDebug    = $config['mail']['SMTPDebug'];
$mail->Host         = $config['mail']['Host'];
$mail->SMTPAuth     = true;
$mail->SMTPSecure   = $config['mail']['SMTPSecure'];
$mail->Port         = $config['mail']['Port'];
$mail->Username     = $config['mail']['Username'];
$mail->Password     = $config['mail']['Password'];

//Recipients
$mail->setFrom($config['mail']['addressOwner'], $config['mail']['nameOwner']); // Owner
$mail->addAddress($config['mail']['addressCustomer'], $config['mail']['nameCustomer']); // Customer
$mail->addBCC($config['mail']['addressAdmin'], $config['mail']['nameAdmin']); // Customer
$mail->addBCC($config['mail']['addressDev'], $config['mail']['nameDev']); // Customer
$mail->addReplyTo($config['mail']['addressOwner'], $config['mail']['nameOwner']); // Reply To

// Encoding and charset
$mail->CharSet = "utf-8";
$mail->Encoding = 'base64';

// Subject and message
$mail->IsHTML(true);
$mail->Subject = $config['mail']['SubjectCustomer'];
$mail->msgHTML(file_get_contents($url_html_email));
$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";

    //if (save_mail($mail)) {
    //    echo "Message saved!";
    //}
}

//Section 2: IMAP
/*
function save_mail($mail)
{
    $path = "{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail";
    $imapStream = imap_open($path, $mail->Username, $mail->Password);
    $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
    imap_close($imapStream);
    return $result;
}
*/