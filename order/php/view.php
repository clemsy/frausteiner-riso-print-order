<?php
$jsonfile = file_get_contents("http://".$_SERVER['HTTP_HOST']."/order/orders/order-".$_GET['order'].".json");

$jsonIterator = new RecursiveIteratorIterator(
    new RecursiveArrayIterator(json_decode($jsonfile, TRUE)),
    RecursiveIteratorIterator::SELF_FIRST);

foreach ($jsonIterator as $key => $value) {
    if(is_array($value)) {
        echo "$key:\n";
    }
    else {
        echo "$key => $value\n";
    }
}
?>
<hr>
<pre>
    <?php print_r(json_decode($jsonfile)); ?>
</pre>