
<!-- cart view -->
<div class="col-sm-3">
    <section class="section" id="results" v-if="form.types!=''">
        <h2 class="is-size-2 has-text-primary	">Your cart</h2>
        <div class="box">
            <ul>
                <li v-for="(item, k) in form">
                    <strong>{{ k | capitalize }}:</strong> {{ item }}
                </li>
            </ul>
            <!--
            -->
            <a class="">show form debug</a>
            <pre class="hidden" id="debugForm">{{ form }}</pre>
            <hr>
            <a class="">show config debug</a>
            <pre class="hidden" id="debugConfig">{{ config }}</pre>
        </div>
    </section>
</div>
<!-- /cart view -->