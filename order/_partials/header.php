<!DOCTYPE html>
<html>
<head>
    <title>Frau Steiner - Order Form</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.4/css/bulma.min.css">
    <link rel="stylesheet" href="./modules/bulma-checkradio/dist/css/bulma-checkradio.min.css">
</head>
<body>

<nav class="navbar is-transparent">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            Frau Steiner
        </a>
        <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div id="navbarExampleTransparentExample" class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item" href="/">
                Home
            </a>
            <a class="navbar-item" href="order.php">
                Order your riso prints
            </a>
            <a class="navbar-item" href="page.html">
                Page
            </a>
            <a class="navbar-item" href="print.html">
                Print
            </a>
        </div>
    </div>
</nav>

<div class="column">

    <h1 class="title">Order online your <strong>riso prints</strong>!</h1>
    <hr>

</div>