
<!-- /main form -->
<div class="column is-two-thirds">


    <section class="section">

        <!-- form starts here -->
        <section class="form">
            <form>


                <h1 class="is-size-2">{{ config.steps[form.step-1] }}</h1>
                <div  class="field has-text-right">
                    <a class="" @click.prevent="prev()" v-if="form.step>1">&larr; Previous</a>
                    <a class="" @click.prevent="next()" v-if="form.step<5">Next &rarr;</a>
                </div>

                <?php include('./_partials/_step1.html'); ?>

                <?php include('./_partials/_step2.html'); ?>



            </form>
        </section>
    </section>
</div>