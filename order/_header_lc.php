<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Order your RISO Prints - Frau Steiner</title>

    <!-- Required Stylesheets --><!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//cdn.clm/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.clm/css/vfg.css">

    <link rel="stylesheet" href="./css/custom.css">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #fff;">
    <a class="navbar-brand" href="#">Frau Steiner</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse  justify-content-between" id="navbarSupportedContent">
        <div class="navbar-nav"></div>
        <div class="navbar-nav">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Order <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">

            </li>


            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Guides
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="/guides/papers.html" data-remote-target="#remoteContentModal .modal-body">papers</a>
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="/guides/formats.html" data-remote-target="#remoteContentModal .modal-body">formats</a>
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="/guides/colors.html" data-remote-target="#remoteContentModal .modal-body">colors</a>
                    <a class="nav-link" href="#remoteContentModal"
                       data-toggle="modal" data-load-remote="/guides/files.html" data-remote-target="#remoteContentModal .modal-body">prepare your files</a>

                </div>
            </li>
            <li class="nav-item">
                <a id="openSpecial" href="#modalSpecial" class="nav-link" data-toggle="modal" data-target="#modalSpecial">Special Offers</a>
            </li>
        </ul>
        </div>
    </div>
</nav>

<div class="contenair-fluid">
    <hr>
</div>