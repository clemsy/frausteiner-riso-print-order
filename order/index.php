<?php include('./_header.php'); ?>


<div class="container-fluid">

    <!-- #app -->
    <div id="app" class="row ml-1 mr-2">

        <!-- cart view -->
        <?php include('./_forms/cart.php'); ?>
        <!-- /cart view -->

        <!-- /main content right -->
        <div class="col-7 col-xs-12">

                <!-- form starts here -->
                <section class="form">
                    <form v-on:submit.prevent="sendOrder" method="POST" action="/post.php" role="form" id="orderForm" enctype="multipart/form-data">
                        <input type="hidden" v-on:change="doQuotation" v-model="changes" id="changes" name="changes" value="0">
                        <div class="float-right" v-if="form.step<=3">
                            <button class="btn magenta" @click.prevent="prev()" v-if="form.step>1">&larr; Previous</button>
                            <button class="btn magenta" @click.prevent="next()">Next &rarr;</button>
                        </div>

                        <h3 class="pl-4">{{ config.steps[form.step-1] |capitalize}}<!--  --></h3>
                        <hr>


                        <fieldset class="pl-4 pt-4" >

                        <div id="formErrorMessages" class="alert alert-danger" role="alert" v-if="form.errors > 0">
                            <h3>Order from has some errors</h3>
                            <ul>
                                <li v-for="(error, k) in form.errorMessages" v-if="error==true">
                                    {{ k }} : the field value is not correct.
                                </li>
                            </ul>
                        </div>

                        <div v-if="form.step === 1">
                            <?php include('./_forms/step1.php'); ?>
                        </div>
                        <div v-if="form.step === 2">
                            <?php include('./_forms/step2.php'); ?>
                        </div>
                        <div v-if="form.step === 3">
                            <?php include('./_forms/delivery.php'); ?>
                        </div>
                        <div v-if="form.step === 4">
                            <?php include('./_forms/payment.php'); ?>
                        </div>
                    </fieldset>
                    </form>
                </section>
        </div>
        <!-- /main  -->



        <div class="modal" tabindex="-1" role="dialog" id="modalSpecial">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Special Offers</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div v-for="index in 4" class="card float-left mr-1 ml-2 mt-1 mb-2" style="width: 23rem;padding:5%;">
                            <h3>Special offer {{index+1}}</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam libero erat, porttitor nec consectetur vitae, maximus vitae est.

                            </p>
                            <button class="btn magenta" type="button" data-dismiss="modal">Oui!!!!!!</button>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn magenta" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- / #app -->

</div>

<hr>
<?php include('./_footer.php'); ?>
