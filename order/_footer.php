
<div class="modal" tabindex="-1" role="dialog" id="remoteContentModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>


<style>
    /* Sticky footer styles
--------------------------------------------------

    html {
        position: relative;
        min-height: 100%;
    }
    body {
        margin-bottom: 80px;
    }
    .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 80px;
        line-height: 40px;

    }
    */
    .footer {

        border-top: 1px solid  #f5f5f5;
        font-size: 0.85em;
        font-weight: 400;
        line-height: 1.8em;

    }
    .footer a{
       text-decoration: underline;
    }
    .footer a:hover{
       text-decoration: none;
        color: #000;
    }
</style>
<footer class="footer mt-5 mb-5 pt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
            <p>
                <a href="tel:+32483657880">call us</a> | <a href="mailto:hallo@frausteiner.be?Subject=hallo%20meine%20liebe" target="_top">email us</a> | <a target="_blank" href="https://www.instagram.com/frausteinerstudio">instagram</a> | <a target="_blank" href="https://www.facebook.com/frausteinerstudio">facebook</a>
            <br>
                Coded by <a target="_blank" href="http://www.c77.be">c77</a> | Font by <a target="_blank" href="http://www.ortype.is">Ortype</a> | All Rights Reserved © 2018 Frau Steiner | <a href="http://www.frausteiner.be/terms/">Terms and Conditions</a>
            </p>
            </div>
        </div>

    </div>
</footer>
<!-- Required scripts -->

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vee-validate@2.0.0-rc.18/dist/vee-validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue2-filters/dist/vue2-filters.min.js"></script>
<script src="https://unpkg.com/vue-form-generator@2.2.2/dist/vfg.js"></script>
<script src="https://unpkg.com/vue-cookies@1.5.5/vue-cookies.js"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsZbRNQabdrTr3mOVGGVgol9JWzUnoqyw&libraries=places">
</script>
<script src="./js/order.js"></script>



<script type="text/javascript">
    $(document).ready(function(){

        $('[data-load-remote]').on('click',function(e) {
            e.preventDefault();
            var $this = $(this);
            var remote = $this.data('load-remote');
            if(remote) {
                $($this.data('remote-target')).load(remote);
            }
        });

        $(document.body).on('hidden.bs.modal', function () {
            $("#changes").attr('value', $("#changes").attr('value')+1);
            console.log('modal hidden')
        })

        // $( "#openSpecial" ).trigger( "click" );

        $('a.btnModalRemote').on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $(".modal-body").html('<iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true" src="'+url+'"></iframe>');
        });

    });

</script>

<script>
    // Warning before leaving the page (back button, or outgoinglink)
    /*
    window.onbeforeunload = function() {
        return "Do you really want to leave our brilliant application?";
        //if we return nothing here (just calling return;) then there will be no pop-up question at all
        //return;
    };
    */
</script>

</body>
</html>